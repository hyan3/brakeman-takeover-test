require 'json'

def compare_bug_reports(file1_path, file2_path)
  json_data1 = JSON.parse(File.read(file1_path))
  json_data2 = JSON.parse(File.read(file2_path))

  matches = 0
  mismatches = 0

  missing = Array.new
  matched = Array.new
  json_data1['vulnerabilities'].each do |v1|
    isMatch = false
    json_data2['vulnerabilities'].each do |v2|
      next if v1['locations']&['file'] != v2['locations']&['file']
      if v1['location'] && v2['location'] &&
        v1['location']['start_line'] && v2['location']['start_line'] &&
        (v1['location']['start_line'] - v2['location']['start_line']).abs <= 1
        isMatch = true
        break
      end
      if v1['location'] && v2['location'] && v1['location']['end_line'] &&
        v1['location']['start_line'] && v2['location']['start_line'] &&
        v1['location']['start_line'] <= v2['location']['start_line'] &&
        v2['location']['start_line'] <= v1['location']['end_line']
        isMatch = true
        break
      end
      if v2['location'] && v1['location'] && v2['location']['end_line'] &&
        v2['location']['start_line'] && v1['location']['start_line'] &&
        v2['location']['start_line'] <= v1['location']['start_line'] &&
        v1['location']['start_line'] <= v2['location']['end_line']
        isMatch = true
        break
      end
    end
    if isMatch
      matches += 1
      matched << v1
    else
      mismatches += 1
      missing << v1
    end
  end

  matched

  puts "matched report id's:"
  matched.each_index do |i|
    puts "#{i}  #{matched[i]['id']}"
  end

  puts "\n\nmismatched report id's:"
  missing.each_index do |i|
    puts "#{i}  #{missing[i]['id']}  @@ #{missing[i]['location']['file']}  @@ #{missing[i]['location']['start_line']} "
  end

  puts "matches: #{matches}"
  puts "mismatches: #{mismatches}"

  json_string = JSON.generate({matches: matched, mismatches: missing})
  File.open('diff.json', 'w') { |file| file.write(json_string) }
end

file1_path = 'gl-sast-report-brakeman.json'
file2_path = 'gl-sast-report-semgrep.json'
compare_bug_reports(file1_path, file2_path)
